# Computational Photography Project
Team 5: Ewan André Golfier, Ulysse Widmer, Mariia Eremina

## Project 3: Feature Visualization - Robust Neural Networks
### Literature review
You can find a literature review in the corresponding folder. There is (should be) a list of the papers we read as well as short descriptions in [`literature-review/README.md`](literature-review/README.md).

### Meeting notes
We meet every Friday from 15:00 to 17:00 in INM 010. You can find short meetings notes and todo list from week to week in [`meetings.md`](meetings.md).

### Project Notebook
The code of the project can be found in `project.ipynb` which can be run locally as a Jupyter Notebook. The notebook will download the robust resnet34 model from [Robust.Art](http://robust.art) (~250MB)

### DATA FOLDER
The data folder will generate, populate and maintain itself automagically when you run the notebook. Don't push it on Git please.

**A downloadable version of the generated data is available here :** https://drive.belle-ferme.ch/index.php/s/XdrixzBNbeqtHiS/download/data.zip
